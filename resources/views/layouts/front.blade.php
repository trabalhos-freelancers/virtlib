<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    @section('css')
    @show
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <title>{{ $appTitle }}</title>
</head>

<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-4">
                <section class="logo">
                    <img src="{{ asset('img/logounesc.png') }}" height="180px;" width="300px;" style="margin-top: -3em;" onclick="javascript:location.href='/'">
                </section>
            </div>
            <div class="col text-right">
                <div class="app-name">
                    <h1 style="text-transform: uppercase; color: #666666;">REPOSITÓRIO DIGITAL</h1>
                    <h2 style="text-transform: uppercase; color: #666666;">{{ date('Y') }} - {{ date('Y') }}</h2>
                </div>
            </div>
        </div>
    </div>
    <section class="subheader">
        <div class="container">
            <div class="row">
                <div class="col-2">
                    <h6 class="subheader-title text-center">
                        Bem-vindo ao nosso <br />
                        acervo digital de artigos
                    </h6>
                </div>
                <div class="col" style="padding-top: 1.2em;">
                    <div class="row justify-content-end">
                    @include('layouts.includes.formulario-geral-pesquisa')
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="subheader-pesquisa">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-8">
                    <ul class="nav" style="padding-top: 0.3em; padding-left: 13.6em">
                        <li>
                            <h6 class="subheader-title">Buscar em todo nosso repositório</h6>
                        </li>
                        <li class="nav-item custom-link">
                            <a class="nav-link" href="{{ route('public.search.authors') }}">Autor</a>
                        </li>
                        <li class="nav-item custom-link">
                            <a class="nav-link" href="{{ route('public.search.advisors') }}">Orientador</a>
                        </li>
                        <li class="nav-item custom-link">
                            <a class="nav-link" href="{{ route('public.search.titles') }}">Título</a>
                        </li>
                        {{--<li class="nav-item custom-link">
                            <a class="nav-link" href="{{ route('public.search.dates') }}">Data</a>
                        </li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </section>
</header>

<div class="container">
    <div class="row">
        <main id="main-content" class="col">
            @yield('content')
        </main>
    </div>
</div>

<footer></footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl"
        crossorigin="anonymous"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

@section('js')
@show
</body>

</html>