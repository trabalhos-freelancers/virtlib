<div class="card card-body custom-box">
    <nav class="nav flex-column">
        <a id="dashboard" class="nav-link" href="/administracao">Dashboard</a>
        <a id="areas-de-conhecimento" class="nav-link" href="{{ route('areas-de-conhecimento.index') }}">Área de Conhecimento</a>
        <a id="tipos-de-publicacao" class="nav-link" href="{{ route('tipos-de-publicacao.index') }}">Tipos de Publicação</a>
        <a id="autores" class="nav-link" href="{{ route('autores.index') }}">Autor</a>
        <a id="orientadores" class="nav-link" href="{{ route('orientadores.index') }}">Orientador</a>
        <a id="assuntos" class="nav-link" href="{{ route('assuntos.index') }}">Assunto</a>
        <a class="nav-link" href="{{ route('publicacoes.index') }}">Publicações</a>
        <nav class="nav flex-column" style="margin-left: 15px">
            <a id="publicacoes" class="nav-link" href="{{ route('publicacoes.index') }}">Listar Todas</a>
            <a id="publicacoes-adicionar" class="nav-link" href="{{ route('publicacoes.create') }}">Adicionar</a>
        </nav>
        <a id="usuarios" class="nav-link" href="{{ route('usuarios.index') }}">Usuários</a>
    </nav>
</div>