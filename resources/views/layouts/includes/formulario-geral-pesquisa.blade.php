<form class="form-inline" action="{{ route('public.search.titles') }}" method="GET">

    <select class="form-control" name="area" id="knowledge-area">
        <option></option>
        <optgroup label="Área de Conhecimento">
            @foreach ($knowledge_areas as $area)
                <option value="{{ $area->id }}">{{ $area->name }}</option>
            @endforeach
        </optgroup>
    </select>

    &nbsp;
    &nbsp;

    <select class="form-control" name="tipo" id="publication_type">
        <option></option>
        <optgroup label="Tipo de publicação">
            @foreach ($publication_types as $type)
                <option value="{{ $type->id }}">{{ $type->name }}</option>
            @endforeach
        </optgroup>
    </select>

    &nbsp;
    &nbsp;

    <div class="input-group">
        <input class="form-control py-1 border-right-0 border" type="search" id="example-search-input" placeholder="Buscar no acervo digital"
            name="filtro">
        <span class="input-group-append">
            <button class="btn btn-outline-secondary border-left-0 border" type="submit">
                <i class="fa fa-search"></i>
            </button>
        </span>
    </div>
</form>