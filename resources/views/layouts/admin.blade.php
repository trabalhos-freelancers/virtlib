<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    @section('css')
    @show
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <title>{{ $appTitle }}</title>
</head>

<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <section class="logo">
                        <img src="{{ asset('img/logounesc.png') }}" height="180px;" width="300px;" style="margin-top: -3em;" onclick="javascript:location.href='/administracao'">
                    </section>
                </div>
                <div class="col text-right">
                    <div class="app-name">
                        <h1 style="text-transform: uppercase; color: #666666;">REPOSITÓRIO DIGITAL</h1>
                        <h2 style="text-transform: uppercase; color: #666666;">{{ date('Y') }} - {{ date('Y') }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <section class="subheader">
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <h2 class="subheader-title">Painel administrativo</h2>
                    </div>
                    <div id="menu-lateral" class="col" style="padding-top: 1.2em;">
                        <div class="row justify-content-end">
                            <div class="row">
                                <ul class="nav" style="padding-right: 2em;">
                                    <li class="nav-item">
                                        <a class="nav-link">{{ Auth::user()->name }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link logout" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" >
                                        Sair</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="post">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </header>

    <div class="container">
        <div class="row">
            <section id="menu-lateral" class="col-3">
                @include('layouts.includes.admin.menu-lateral')
            </section>

            <main class="col">
                @section('breadcrumb')
                    @show

                @yield('content')
            </main>
        </div>
    </div>

    <footer></footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl"
        crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script>
        /** acertando menu dinamicamente */
        var fatias = (document.location.pathname).split("/");
        
        if ( fatias.length < 5) {
            var menu_ativo = fatias[3];

            if ( typeof fatias[2] == 'undefined' )
                menu_ativo = 'dashboard';
                
            if ( fatias[2] == 'publicacoes' )
                menu_ativo = 'publicacoes';

            if ( (fatias[2] == 'publicacoes') && ( fatias[3] == 'adicionar') )
                menu_ativo = 'publicacoes-' + fatias[3];

            menu_ativo = '#' + menu_ativo;
            $(menu_ativo).addClass('active');
        } else {
            var menu_ativo = fatias[4];

            if ( typeof fatias[3] == 'undefined' )
                menu_ativo = 'dashboard';
                
            if ( fatias[3] == 'publicacoes' )
                menu_ativo = 'publicacoes';

            if ( (fatias[3] == 'publicacoes') && ( fatias[4] == 'adicionar') )
                menu_ativo = 'publicacoes-' + fatias[4];

            menu_ativo = '#' + menu_ativo;
            $(menu_ativo).addClass('active');
        }
    </script>

    @section('js')
    @show
</body>

</html>