<form id="form-delete-publication_type" method="POST">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>