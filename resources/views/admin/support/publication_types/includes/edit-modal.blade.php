<!-- Modal -->
<div class="modal fade" id="publication_type-edit-modal" tabindex="-1" role="dialog" aria-labelledby="publication_type-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="publication_type-edit-modal-label">Atualizar Tipo de Publicação</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-edit-publication_type" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <div class="form-group"> 
                        <label for="publication_type-name">Nome</label>
                        <input type="text" class="form-control rounded-0" placeholder="Tipo de Publicação" name="name" id="publication_type-name">   
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>