<form id="form-delete-advisor" method="POST">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>