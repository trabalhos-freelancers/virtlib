<!-- Modal -->
<div class="modal fade" id="author-edit-modal" tabindex="-1" role="dialog" aria-labelledby="author-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="author-edit-modal-label">Atualizar Autor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-edit-author" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <div class="form-group"> 
                        <label for="author-name">Nome</label>
                        <input type="text" class="form-control rounded-0" placeholder="Nome" name="name" id="author-name">   
                    </div>
                    <div class="form-group"> 
                        <label for="author-last-name">Sobrenome</label>
                        <input type="text" class="form-control rounded-0" placeholder="Sobrenome" name="last_name" id="author-last-name">   
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>