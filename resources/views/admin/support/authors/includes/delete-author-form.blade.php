<form id="form-delete-author" method="POST">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>