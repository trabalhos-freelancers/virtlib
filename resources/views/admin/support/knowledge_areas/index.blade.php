@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/administracao">Administração</a></li>
            <li class="breadcrumb-item active" aria-current="page">Áreas de Conhecimento</li>
        </ol>
    </nav>
@endsection

@section('content')
<h3 class="page-title">
    <b>Lista de Áreas de Conhecimento</b>
</h3>

<div class="card custom-box" style="margin-bottom: 12px;">
    <div class="card-body text-right">
        <form id="form-new-knowledge_area" action="{{ route('areas-de-conhecimento.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-row">
                <div class="col-7">
                    <input type="text" class="form-control rounded-0" placeholder="Área de Conhecimento" name="name" required>
                </div>
                <div class="col">
                    <button id="btn-add" type="submit" class="btn btn-block bg-custom-red rounded-0">
                        Adicionar Área de Conhecimento
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col">
        <table id="table-knowledge_areas" class="table table-sm table-bordered">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th class="text-center">Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($knowledge_areas as $knowledge_area)
                    <tr id="knowledge_area-{{ $knowledge_area->id }}">
                        <td>{{ $knowledge_area->name }}</td>
                        <td class="text-center" width="65px;">
                            @if ( count($knowledge_area->publications ) )
                                <a href="{{ route('publicacoes.index') }}?modelo=area&id={{ $knowledge_area->id}}" class="custom-link-total">
                                    {{ count( $knowledge_area->publications ) }}
                                </a>
                            @else
                                N/I
                            @endif
                        </td>
                        <td class="text-center" width="170px;">
                            <button class="btn btn-sm btn-warning"
                                onclick="knowledge_areaEdit({{ $knowledge_area->id }})">
                                <i class="fa fa-fw fa-edit"></i>
                                editar
                            </button>
                            
                            <button class="btn btn-sm btn-danger"
                                onclick="knowledge_areaDelete({{ $knowledge_area->id }})">
                                <i class="fa fa-fw fa-trash"></i>
                                deletar
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@include('admin.support.knowledge_areas.includes.edit-modal')
@include('admin.support.knowledge_areas.includes.delete-knowledge_area-form')

@stop 

@section('css')
    <link rel="stylesheet" href="http://cdn.jsdelivr.net/npm/jquery-easy-loading/dist/jquery.loading.min.css">
@stop

@section('js')
    <script>
        $(document).ready(function() {
            $('#table-knowledge_areas').DataTable({
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
                },
            });
        } );
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-easy-loading@1.3.0/dist/jquery.loading.min.js"></script>
    <script src="{{ asset('js/controllers/knowledge-areas.js') }}"></script>
@stop