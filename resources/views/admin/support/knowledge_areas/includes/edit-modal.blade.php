<!-- Modal -->
<div class="modal fade" id="knowledge_area-edit-modal" tabindex="-1" role="dialog" aria-labelledby="knowledge_area-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="knowledge_area-edit-modal-label">Atualizar Área de Conhecimento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-edit-knowledge_area" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <div class="form-group"> 
                        <label for="knowledge_area-name">Nome</label>
                        <input type="text" class="form-control rounded-0" placeholder="Área de Conhecimento" name="name" id="knowledge_area-name">   
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>