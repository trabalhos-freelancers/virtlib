<form id="form-delete-knowledge_area" method="POST">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>