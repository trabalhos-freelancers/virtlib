@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/administracao">Administração</a></li>
            <li class="breadcrumb-item active" aria-current="page">Assuntos</li>
        </ol>
    </nav>
@endsection

@section('content')
<h3 class="page-title">
    <b>Lista de Assuntos</b>
</h3>

<div class="card custom-box" style="margin-bottom: 12px;">
    <div class="card-body text-right">
        <form id="form-new-subject" action="{{ route('assuntos.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-row">
                <div class="col-8">
                    <input type="text" class="form-control rounded-0" placeholder="Assunto" name="name" required>
                </div>
                <div class="col">
                    <button id="btn-add" type="submit" class="btn btn-block bg-custom-red rounded-0">
                        Adicionar Assunto
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col">
        <table id="table-subjects" class="table table-sm table-bordered">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th class="text-center">Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($subjects as $subject)
                    <tr id="subject-{{ $subject->id }}">
                        <td>{{ $subject->name }}</td>
                        <td class="text-center" width="65px;">
                            @if ( count($subject->publications ) )
                                <a href="{{ route('publicacoes.index') }}?modelo=subject&id={{ $subject->id}}" class="custom-link-total">
                                    {{ count( $subject->publications ) }}
                                </a>
                            @else
                                N/I
                            @endif
                        </td>
                        <td class="text-center" width="170px;">
                            <button class="btn btn-sm btn-warning"
                                onclick="subjectEdit({{ $subject->id }})">
                                <i class="fa fa-fw fa-edit"></i>
                                editar
                            </button>
                            
                            <button class="btn btn-sm btn-danger"
                                onclick="subjectDelete({{ $subject->id }})">
                                <i class="fa fa-fw fa-trash"></i>
                                deletar
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@include('admin.support.subjects.includes.edit-modal')
@include('admin.support.subjects.includes.delete-subject-form')

@stop 

@section('css')
    <link rel="stylesheet" href="http://cdn.jsdelivr.net/npm/jquery-easy-loading/dist/jquery.loading.min.css">
@stop

@section('js')
    <script>
        $(document).ready(function() {
            $('#table-subjects').DataTable({
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
                },
            });
        } );
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-easy-loading@1.3.0/dist/jquery.loading.min.js"></script>
    <script src="{{ asset('js/controllers/subjects.js') }}"></script>
@stop