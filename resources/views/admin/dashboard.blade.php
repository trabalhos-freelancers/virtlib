@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Administração</a></li>
            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col">
            <div class="card custom-box text-center mb-3">
                <div class="card-header bg-custom-blue">Usuários</div>
                <div class="card-body">
                    <p class="card-text">Total: {{ $users_count }}</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card custom-box text-center mb-3">
                <div class="card-header bg-custom-yellow">Publicações</div>
                <div class="card-body">
                    <p class="card-text">Total: {{ $publications_count }}</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card custom-box text-center mb-3">
                <div class="card-header bg-custom-red">Arquivos</div>
                <div class="card-body">
                    <p class="card-text">Total: {{ $files_count }}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card card-body">
                <table class="table table-sm table-bordered">
                    <thead class="bg-custom-red">
                        <tr class="text-center">
                            <th colspan="3">Últimas publicações adicionadas</th>
                        </tr>
                    </thead>
                    <thead>
                            <tr>
                                <th>Título</th>
                                <th>Author</th>
                                <th class="text-center">Ano</th>
                            </tr>
                        </thead>
                    <tbody>
                        @foreach ($last_publications as $publication)
                            <tr>
                                <td>{{ $publication->title }}</td>
                                <td>{{ $publication->author->name }}</td>
                                <td class="text-center" width="50px">{{ $publication->year }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection