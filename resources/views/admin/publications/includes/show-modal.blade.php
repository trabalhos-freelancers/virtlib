<!-- Modal -->
<div class="modal fade" id="publication-show-modal" tabindex="-1" role="dialog" aria-labelledby="publication-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="publication-edit-modal-label">Dados da Publicação</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="container-fluid" style="margin-top: 12px;">
                <div class="row">
                    <div class="col">
                        <section id="publication-info"></section>
                    </div>
                    <div class="col">
                        <section id="publication-summary"></section>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>