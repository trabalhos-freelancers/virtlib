<form id="form-delete-publication-file" method="POST">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>