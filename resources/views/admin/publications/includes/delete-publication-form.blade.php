<form id="form-delete-publication" method="POST">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>