@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/administracao">Administração</a></li>
            <li class="breadcrumb-item"><a href="{{ route('publicacoes.index') }}">Publicações</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edição</li>
        </ol>
    </nav>
@endsection

@section('content')
    <h3 class="page-title">
        <b>Editar Publicação "{{ $publication->title }}"</b>
    </h3>

    <div class="card" style="margin-top: 12px;">
        <form id="form-publication" action="{{ route('publicacoes.update', ['id' => $publication->id]) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Título</label>
                    <input class="form-control" name="title" id="title" value="{{ $publication->title }}">
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="author">Autor</label>
                            <select class="form-control" name="author" id="author">
                                <option></option>
                                @foreach ($authors as $author)
                                    <option value="{{ $author->id }}" @if($publication->author->id === $author->id) selected @endif>{{ $author->name }} {{ $author->last_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="advisor">Orientador</label>
                            <select class="form-control" name="advisor" id="advisor">
                                <option value="n/i"></option>
                                @foreach ($advisors as $advisor)
                                    <option value="{{ $advisor->id }}" @if(($publication->advisor) && ($publication->advisor->id === $advisor->id)) selected @endif>{{ $advisor->name }} {{ $advisor->last_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="knowledge-area">Área de Conhecimento</label>
                            <select class="form-control" name="knowledge_area" id="knowledge-area">
                                <option>Selecione...</option>
                                @foreach ($knowledge_areas as $area)
                                    <option value="{{ $area->id }}" @if($publication->knowledgeArea->id === $area->id) selected @endif>{{ $area->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="publication-type">Tipo de Publicação</label>
                            <select class="form-control" name="publication_type" id="publication-type">
                                <option>Selecione...</option>
                                @foreach ($publication_types as $type)
                                    <option value="{{ $type->id }}" @if($publication->type->id === $type->id) selected @endif>{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="year">Ano</label>
                            <input type="number" min="0" xlength="" class="form-control" name="year" value="{{ $publication->year }}" id="year">
                        </div>
                    </div>
                    <div class="col">
                        <div class="card card-body" style="margin-top:30px;max-height: 24.1em;overflow-y: scroll;">
                            @foreach ($subjects as $subject)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="subjects[]" value="{{ $subject->id }}" id="subject-{{ $subject->id }}"
                                    @if( in_array($subject->id, $publication->subjects()->pluck('subject_id')->all()) ) checked @endif>
                                    <label class="form-check-label" for="subject-{{ $subject->id }}">
                                        {{ $subject->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="editor">Resumo da publicação</label>
                    <textarea class="form-control" id="edito" name="summary">{{ $publication->summary }}</textarea>
                </div>

                @if ( (count($publication->files) - 3) )
                    <div class="form-group">
                        <label class="control-label">Selecione os Arquivos da Publicação</label>
                        <input id="files" name="files[]" type="file" multiple>
                    </div>
                @endif

                <div class="card card-body">
                    <table class="table table-sm table-bordered">
                        <thead class="bg-dark" style="color: whitesmoke;">
                            <tr>
                                <th colspan="2" class="text-center">Arquivos associados</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($publication->files as $file)
                            <tr>
                                <td>
                                    <b>{{ $file->name }}</b>
                                </td>
                                <td class="text-center" style="width: 80px;">
                                    <button type="button" class="btn btn-sm btn-danger"
                                            onclick="publicationFileDelete({{ $file->id }})">
                                        <i class="fa fa-fw fa-trash">&nbsp;</i> deletar
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="card-footer text-right">
                    <button type="button" class="btn btn-danger"
                            onclick="javascript:document.location.href='{{ route('publicacoes.index') }}'">
                        Cancelar
                    </button>
                    <button type="submit" class="btn btn-success">Atualizar Publicação</button>
                </div>
            </div>
        </form>

        @include('admin.publications.includes.delete-publication-file-form')
    </div>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="http://cdn.jsdelivr.net/npm/jquery-easy-loading/dist/jquery.loading.min.css">
    <link href="{{ asset('css/fileinput.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/jquery-easy-loading@1.3.0/dist/jquery.loading.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset('js/fileinput.min.js') }}"></script>
    <script src="{{ asset('themes/fas/theme.js') }}"></script>
    <script src="{{ asset('js/locales/pt-BR.js') }}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-beta.1/classic/ckeditor.js"></script>
    <script src="{{ asset('js/controllers/publications.js') }}"></script>
    <script>
        $('select').select2({
            theme: "bootstrap4"
        });

        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );

        $("#files").fileinput({
            theme: "fas",
            language: 'pt-BR',
            showUpload: false,
            dropZoneEnabled: false,
            allowedFileExtensions: ["pdf"],
            maxFileCount: 3
        });
    </script>
@endsection