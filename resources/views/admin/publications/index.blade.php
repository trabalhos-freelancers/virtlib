@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Administração</a></li>
            <li class="breadcrumb-item active" aria-current="page">Publicações</li>
        </ol>
    </nav>
@endsection

@section('content')
    <h3 class="page-title">
        <b>Lista de Publicacoes</b>
    </h3>

    <br />

    <div class="row">
        <div class="col">
            <table id="table-publications" class="table table-sm table-bordered">
                <thead>
                <tr>
                    <th>Título</th>
                    <th>Autor</th>
                    <th>Ano</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($publications as $publication)
                    <tr id="publication-{{ $publication->id }}">
                        <td>{{ $publication->title }}</td>
                        <td>{{ $publication->author->name }}</td>
                        <td width="50px">{{ $publication->year }}</td>
                        <td class="text-center" width="170px;">
                            <a role="button" class="btn btn-sm btn-warning"
                               href="{{ route('publicacoes.edit', ['id' => $publication->id]) }}">
                                <i class="fa fa-fw fa-edit"></i>
                            </a>

                            <button class="btn btn-sm btn-info"
                                    onclick="publicationShow({{ $publication->id }})">
                                <i class="fa fa-fw fa-eye"></i>
                            </button>

                            <button class="btn btn-sm btn-danger"
                                    onclick="publicationDelete({{ $publication->id }})">
                                <i class="fa fa-fw fa-trash"></i>
                                deletar
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('admin.publications.includes.delete-publication-form')
    @include('admin.publications.includes.show-modal')
@stop

@section('css')
    <link rel="stylesheet" href="http://cdn.jsdelivr.net/npm/jquery-easy-loading/dist/jquery.loading.min.css">
@stop

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/jquery-easy-loading@1.3.0/dist/jquery.loading.min.js"></script>
    <script src="{{ asset('js/controllers/publications.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#table-publications').DataTable({
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
                },
            });
        } );
    </script>
@stop