<form id="form-delete-user" method="POST">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>