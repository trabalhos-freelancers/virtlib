@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/usuarios">Administração</a></li>
            <li class="breadcrumb-item active" aria-current="page">Usuários</li>
        </ol>
    </nav>
@endsection

@section('content')
<h3 class="page-title">
    <b>Lista de Usuários</b>
</h3>

<div class="card custom-box" style="margin-bottom: 12px;">
    <div class="card-body text-right">
        <form id="form-new-user" action="{{ route('usuarios.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input type="text" class="form-control rounded-0" placeholder="Nome" name="name" required>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="email" class="form-control rounded-0" placeholder="Email" name="email" required>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="password" class="form-control rounded-0" placeholder="Senha" name="password" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <button id="btn-add" type="submit" class="btn btn-block bg-custom-red rounded-0">
                            Adicionar Usuário
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col">
        <table id="table-users" class="table table-sm table-bordered">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Email</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr id="user-{{ $user->id }}">
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td class="text-center" width="170px;">
                            <button class="btn btn-sm btn-warning"
                                onclick="userEdit({{ $user->id }})">
                                <i class="fa fa-fw fa-edit"></i>
                                editar
                            </button>
                            
                            <button class="btn btn-sm btn-danger"
                                onclick="userDelete({{ $user->id }})">
                                <i class="fa fa-fw fa-trash"></i>
                                deletar
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@include('admin.users.includes.edit-modal')
@include('admin.users.includes.delete-user-form')

@stop 

@section('css')
    <link rel="stylesheet" href="http://cdn.jsdelivr.net/npm/jquery-easy-loading/dist/jquery.loading.min.css">
@stop

@section('js')
    <script>
        $(document).ready(function() {
            $('#table-users').DataTable({
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
                },
            });
        } );
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-easy-loading@1.3.0/dist/jquery.loading.min.js"></script>
    <script src="{{ asset('js/controllers/users.js') }}"></script>
@stop