<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">

        <title>Signin Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="{{ asset('css/signin.css') }}" rel="stylesheet">
    </head>

    <body class="text-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-4">
                    <div class="card card-body">
                        <form class="form-signin" action="{{ route('login') }}" method="post">
                            {{ csrf_field() }}
                            <img class="mb-4" src="{{ asset('img/logounesc.png') }}" alt="" width="200">

                            <h1 class="h3 mb-3 font-weight-normal">Autenticação Repositório Digital</h1>

                            <div class="form-group">
                                <label for="inputEmail" class="sr-only">Endereço de Email</label>
                                <input type="email" id="inputEmail" class="form-control" placeholder="Endereço de Email" name="email" required>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword" class="sr-only">Senha</label>
                                <input type="password" id="inputPassword" class="form-control" placeholder="Senha" name="password" required>
                            </div>

                            <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar no sistema</button>
                            <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    </body>
</html>
