@extends('layouts.front')

@section('content')
    <div class="row justify-content-center">
        <div class="col-10">
            <div class="row">
                <div class="col-6">
                    <section id="publication-info">
                        Título: <span class="custom-red">{{ $publication->title }}</span>
                        <b>
                            <br>
                            <b>Data: <span class="custom-red">{{ $publication->year }}</span></b><br>
                            <b>Autor: <span class="custom-red">{{ $publication->author->name }} {{ $publication->author->last_name }}</span></b><br>
                            <b>Orientador: <span class="custom-red">@if( $publication->advisor) {{ $publication->advisor->name}} {{ $publication->advisor->last_name}} @else N/I @endif</span></b><br>
                            <b>Tipo de publicação: <span class="custom-red">{{ $publication->type->name }}</span></b><br>
                            <b>Área de conhecimento: <span class="custom-red">{{ $publication->knowledgeArea->name }}</span></b><br>
                            <b>Assunto:
                                @foreach($publication->subjects as $subject)
                                    <span class="badge badge-danger">{{ $subject->name }}</span>
                                @endforeach
                            </b><br>

                            <hr>
                        </b>

                        @foreach ( $publication->files as $key => $file )
                            <a role="button" class="btn bg-custom-red" target="_blank" style="margin-bottom: 6px;"
                               href="{{ route('public.publications.download', ['path' => explode('/',$file->path)[1]]) }}">
                                <i class="fa fa-fw fa-download">&nbsp</i> Arquivo {{ ++$key }}
                            </a>
                        @endforeach
                    </section>
                </div>
                <div class="col">
                    <section id="publication-summary">
                        <b>Resumo da Publicação</b>
                        <br />
                        {!! $publication->summary !!}
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection