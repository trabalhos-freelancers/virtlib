@extends('layouts.front')

@section('content')
    <div class="row justify-content-center">
        <div class="col-5">
            <form class="text-center">
                <label for="example-search-input" class="front-title" style="color: #990000;font-weight: bold;">Listagem por Publicação</label>
                <div class="input-group">
                    <input class="form-control py-1 border-right-0 border text-center" type="search" id="example-search-input" placeholder="Ou pesquise pelo título da publicação" name="titulo">
                    <span class="input-group-append">
                        <button class="btn btn-outline-secondary border-left-0 border" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>

            <br />

            <ul class="lista">
                @foreach( $publications as $publication )
                    <li>
                        <a href="?filtro={{$publication->title}}">{{ $publication->title }}
                            <small style="color:#990000;text-transform: none;">
                                [{{ $publication->author->name }}, {{ $publication->author->last_name }}]
                            </small>
                        </a>
                    </li>
                @endforeach
            </ul>

            <br />

            {{ $publications->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $('select').select2({
            theme: "bootstrap4"
        });
    </script>
@endsection