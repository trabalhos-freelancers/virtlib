@extends('layouts.front')

@section('content')
<div class="container">
    <div class="row" style="margin-top: 6px;">
        <div class="col-6">
            <img src="{{ asset('img/home-book.jpg') }}">
        </div>

        <div class="col">
            <div class="text-center">
                <h1 class="front-title">Últimas Publicações</h1>
            </div>

            @foreach( $publications as $publication )
                <ul class="publication" onclick="javascript: location.href='{{ route('public.publication.show', ['id' => $publication->id]) }}'">
                    <br />
                    <li class="publication-title">{{ $publication->title }}</li>
                    <li class="publication-author">{{ $publication->author->last_name }}, {{ $publication->author->name }} ({{ $publication->year }})</li>
                    <li class="publication-summary text-justify" style="max-width: 450px;">{{ strip_tags(str_limit($publication->summary, $words = 250, $end = '...')) }}</li>
                    <br/>
                </ul>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('css/select2-bootstrap4.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="http://cdn.jsdelivr.net/npm/jquery-easy-loading/dist/jquery.loading.min.css">
    <link href="{{ asset('css/fileinput.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $('select').select2({
            theme: "bootstrap4"
        });
    </script>
@endsection
