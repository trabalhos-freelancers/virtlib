@extends('layouts.front')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            <br />
            <h3 class="front-title" style="color: #990000;font-weight: bold;">
                Listagem por Orientador
                <small style="color:black;text-transform: none;">
                    {{ $advisor->last_name }}, {{ $advisor->name }} [{{ count($advisor->publications) }}]
                </small>
            </h3>
            @foreach( $publications as $publication )
                <ul class="publication" onclick="javascript: location.href='{{ route('public.publication.show', ['id' => $publication->id]) }}'">
                    <br />
                    <li class="publication-title">{{ $publication->title }}</li>
                    <li class="publication-author">{{ $publication->author->last_name }}, {{ $publication->author->name }} ({{ $publication->year }})</li>
                    <li class="publication-summary text-justify" style="margin-right: 40px;">{{ strip_tags(str_limit($publication->summary, $words = 250, $end = '...')) }}</li>
                    <br/>
                </ul>
            @endforeach

            {{ $publications->links() }}
        </div>
    </div>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('css/select2-bootstrap4.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $('select').select2({
            theme: "bootstrap4"
        });
    </script>
@endsection