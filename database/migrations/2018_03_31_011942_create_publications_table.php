<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id')->unsigned();
            $table->integer('advisor_id')->unsigned()->nullable();
            $table->integer('type_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->string('title');
            $table->mediumText('summary')->nullable();
            $table->unsignedInteger('year');
            $table->timestamps();

            $table->foreign('author_id')
                  ->references('id')->on('authors')
                  ->onDelete('cascade');

            $table->foreign('advisor_id')
                ->references('id')->on('advisors')
                ->onDelete('cascade');

            $table->foreign('type_id')
                ->references('id')->on('publication_types')
                ->onDelete('cascade');

            $table->foreign('area_id')
                ->references('id')->on('knowledge_areas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publications', function(Blueprint $table){
            $table->dropForeign(['author_id']);
            $table->dropForeign(['advisor_id']);
            $table->dropForeign(['type_id']);
            $table->dropForeign(['area_id']);
        });
        Schema::dropIfExists('publications');
    }
}
