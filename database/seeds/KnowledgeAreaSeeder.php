<?php

use Illuminate\Database\Seeder;

class KnowledgeAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Virtlib\Models\KnowledgeArea::class, 15)->create();
    }
}
