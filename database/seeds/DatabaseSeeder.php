<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AuthorSeeder::class);
        $this->call(AdvisorSeeder::class);
        $this->call(SubjectSeeder::class);
        $this->call(PublicationTypeSeeder::class);
        $this->call(KnowledgeAreaSeeder::class);
        $this->call(UserSeeder::class);
    }
}
