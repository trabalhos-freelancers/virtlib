<?php

use Faker\Generator as Faker;

$factory->define(\Virtlib\Models\PublicationType::class, function (Faker $faker) {
    return [
        'name' => $faker->word()
    ];
});
