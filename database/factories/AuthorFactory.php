<?php

use Faker\Generator as Faker;

$factory->define(Virtlib\Models\Author::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'last_name' => $faker->lastName
    ];
});
