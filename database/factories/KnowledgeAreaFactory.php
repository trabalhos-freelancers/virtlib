<?php

use Faker\Generator as Faker;

$factory->define(\Virtlib\Models\KnowledgeArea::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(10)
    ];
});
