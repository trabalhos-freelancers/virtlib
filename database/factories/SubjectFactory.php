<?php

use Faker\Generator as Faker;

$factory->define(\Virtlib\Models\Subject::class, function (Faker $faker) {
    return [
        'name' => $faker->word()
    ];
});
