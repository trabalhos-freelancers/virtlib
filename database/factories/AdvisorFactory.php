<?php

use Faker\Generator as Faker;

$factory->define(\Virtlib\Models\Advisor::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'last_name' => $faker->lastName
    ];
});
