<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthorsTest extends TestCase
{
    public function test_all_available_authors()
    {
        $response = $this->get('/administracao/cadastros/autores');
        $response->assertStatus(200);
        $response->assertViewHas('authors');
        $response->assertSeeText('Lista de Autores');
    }
}
