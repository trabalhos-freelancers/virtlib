<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Virtlib\Repositories\Interfaces\AuthorRepositoryInterface;
use Virtlib\Repositories\AuthorRepository;
use Virtlib\Models\Author;

class AuthorRepositoryTest extends TestCase
{
    use RefreshDatabase;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new AuthorRepository( new Author() );
    }

    public function test_find_author_by_id() 
    {
        /** Faking dummy data */
        $authors = factory(Author::class, 25)->create();

        /** Choosing one and delete them! */
        $id = rand($authors[0]->id, $authors[2]->id);

        $lookingFor = $this->repository->findById( $id );

        $this->assertTrue( ($lookingFor != null) );
        $this->assertDatabaseHas('authors',[
            'id' => $lookingFor->id,
            'name' => $lookingFor->name,
            'last_name' => $lookingFor->last_name
        ]);
    }

    public function test_list_all_authors()
    {
        /** Faking dummy data */
        $authors = factory(Author::class, 25)->create();

        /** Assertion */
        $this->assertCount(25, $authors);
    }

    public function test_create_new_author()
    {
        $data = [
            'name' => 'author',
            'last_name' => 'Test Numer One'
        ];

        $author = $this->repository->create( $data );

        $this->assertDatabaseHas('authors', [
            'id' => $author->id,
            'name' => $author->name,
            'last_name' => $author->last_name
        ]);
    }

    public function test_update_author()
    {
        /** Creating and verifyind the new author */
        /** REFACTOR */
        $data = [
            'name' => 'author',
            'last_name' => 'Test Numer One'
        ];

        $author = $this->repository->create( $data );

        $this->assertDatabaseHas('authors', [
            'id' => $author->id,
            'name' => $author->name,
            'last_name' => $author->last_name
        ]);

        /** Updating them with new datas */
        $updatedData = [
            'name' => '[ updated! ] '.$author->name,
        ];

        $updatedAuthor = $this->repository->findById( $author->id );

        $updatedAuthor = $this->repository->update($updatedAuthor->id, $updatedData);

        /** assertions */
        $this->assertEquals($updatedAuthor->id, $author->id);
        $this->assertDatabaseHas('authors', [
            'id' => $updatedAuthor->id,
            'name' => $updatedAuthor->name,
            'last_name' => $updatedAuthor->last_name
        ]);
    }

    public function test_delete_author()
    {
        /** Faking dummy data */
        $authors = factory(Author::class, 3)->create();

        /** Choosing one and delete them! */
        $deletedId = rand($authors[0]->id, $authors[2]->id);

        $result = $this->repository->delete($deletedId);

        $this->assertTrue( $result );
        $this->assertFalse( ($this->repository->findById($deletedId) != null) );
        $this->assertTrue( (count( $this->repository->listAll() ) === 2) );
    }
}
