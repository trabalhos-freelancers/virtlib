/**
 * Eventos Gerais
 */
$('#form-new-user').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  form.loading({
    "message": "Processando requisição"
  });

  $.post(url, $(this).serialize())
    .done(function (response) {
      swal({
        title: "Tudo ok!",
        text: "Novo usuário foi adicionado com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
      document.querySelector('#form-new-user').reset();
    })
    .fail(function () {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      form.loading('stop');
    });
});

$('#user-edit-modal #form-edit-user').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  $.post(url, $(this).serialize())
    .done(function (response) {
      /** obtendo referencia ao modal */
      var modal = $('#user-edit-modal');
      modal.modal('hide');

      swal({
        title: "Tudo ok!",
        text: "Usuário foi editado com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
    })
    .fail(function () {
      /** obtendo referencia ao modal */
      var modal = $('#user-edit-modal');
      modal.modal('hide');

      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente em alguns instantes.",
        icon: "error",
      });
      form.loading('stop');
    });
});

/**
 * FIM EVENTOS GERALS
 */

/**
 * Funções GERAIS
 */

function userEdit(id) {
  var row = $('#user-' + id);
  var url = location.href;

  url += '/' + id + '/editar';

  row.loading({
    "message": "Carregando usuário..."
  });

  $.get(url)
    .done(function (response) {
      row.loading('stop');

      /** obtendo referencia ao modal */
      var modal = $('#user-edit-modal');

      /** limpando dados anteriores */
      document.querySelector('#user-edit-modal #form-edit-user').reset();

      /** setando action */
      var action =  location.href + '/' + id;
      $('#user-edit-modal #form-edit-user').attr('action', action)

      /** populando dados */
      $('#form-edit-user #user-name').attr('value', response.name);
      $('#form-edit-user #user-email').attr('value', response.email);

      /** finalmente abrindo modal */
      modal.modal('show');
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      row.loading('stop');
    });
}

function userDelete( id ) {
  swal({
    title: "Você tem certeza ?",
    text: "Uma vez deletado você não será capaz de recuperá-lo!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      deleteuserData( id );
    }
  });
}

function deleteuserData(id)
{
  var form = $('#form-delete-user');
  var url = location.href + '/' + id;

  $.post(url, form.serialize())
    .done(function (response) {
      swal("Ok! Usuário deletado com sucesso da base de dados", {
        icon: "success",
      }).then(function(){
        location.reload();
      });
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
    });
}