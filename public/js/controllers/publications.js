/**
 * Eventos Gerais
 */
$('#form-publication').submit(function (event) {
  event.preventDefault();
  var redirect = location.href.split('/');
  redirect = redirect[0] + '//' + redirect[2] + '/' + redirect[3] + '/' + redirect[4];
  
  var url = $(this).attr('action');
  var form = $(this);
  var formData = new FormData($(this)[0]);

  form.loading({
    "message": "Processando requisição"
  });

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        success: function (response) {
            swal({
                title: "Tudo ok!",
                text: response.msg,
                icon: "success",
                showCloseButton: false
            }).then(function () {
                location.href = redirect;
            });
        },
        error:function ( response ) {
            swal({
                title: "Problema!",
                text: "Ocorreu algum erro. Tente novamente.",
                icon: "error",
            });
            form.loading('stop');
            (document.querySelector('#form-publication')).reset()
        },
        cache: false,
        processData: false,
        contentType: false
    });
});

/**
 * FIM EVENTOS GERALS
 */

/**
 * Funções GERAIS
 */

function publicationShow(id) {
  var row = $('#publication-' + id);
  var url = location.href.split("?")[0];

  url += '/' + id;

  row.loading({
    "message": "Carregando dados..."
  });

  $.get(url)
    .done(function (response) {
      row.loading('stop');

      /** obtendo referencia ao modal */
      var modal = $('#publication-show-modal');
      console.log( response );

      /** populando dados obtidos da requisição */
      var info_html = '<b>Título: <span class="custom-red">'+ response.title +'</span><b><br>';
      info_html += "<b>Data: <span class='custom-red'>"+ response.year +"</span></b><br>";
      info_html += "<b>Autor: <span class='custom-red'>"+ response.author.name +" "+ response.author.last_name +"</span></b><br>";
      if ( response.advisor ) { 
          info_html += "<b>Orientador: <span class='custom-red'>"+ response.advisor.name +" "+ response.advisor.last_name +"</span></b><br>";
      }
      else {
          info_html += "<b>Orientador: <span class='custom-red'> N/I </span></b><br>";
      }
      info_html += "<b>Tipo de publicação: <span class='custom-red'>"+ response.type.name +"</span></b><br>";
      info_html += "<b>Área de conhecimento: <span class='custom-red'>"+ response.knowledge_area.name +"</span></b><br>";
      info_html +="<b>Assunto: ";
      response.subjects.forEach( function (subject) {
         info_html += '<span class="badge badge-danger">'+ subject.name +'</span> ';
      });
      info_html += "</b><br><hr />";
      var cont = 1;
      response.files.forEach( function (file) {
          info_html += "<a href='/donwload/"+ file.path.split('/')[1] +"' target='_blank' role='button' class='btn bg-custom-red' style='margin-bottom: 6px;'>" +
              "<i class='fa fa-fw fa-download'>&nbsp;</i>" +
              "Arquivo " + cont +
              "</a> ";
          cont+=1;
      });


      var summary_html = "<b>Resumo</b><br>" + response.summary;

      $('#publication-show-modal #publication-info').html( info_html );
      $('#publication-show-modal #publication-summary').html( summary_html );

      /** finalmente abrindo modal */
      modal.modal('show');
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      row.loading('stop');
    });
}

function publicationDelete( id ) {
  swal({
    title: "Você tem certeza ?",
    text: "Uma vez deletada você não será capaz de recuperá-la!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      deletePublicationData( id );
    }
  });
}

function publicationFileDelete( id ) {
    swal({
        title: "Você tem certeza ?",
        text: "Uma vez deletada você não será capaz de recuperá-la!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            deletePublicationFileData( id );
        }
    });
}

function deletePublicationData(id)
{
  var form = $('#form-delete-publication');
  var url = location.href + '/' + id;

  $.post(url, form.serialize())
    .done(function (response) {
      swal("Ok! Publicação deletada com sucesso da base de dados", {
        icon: "success",
      }).then(function(){
        location.reload();
      });
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
    });
}

function deletePublicationFileData(id)
{
    var form = $('#form-delete-publication-file');
    var url = location.href.split('/');

    if ( url.length < 8 ) {
        url = url[0] + '//' + url[2] + '/' + url[3] + '/arquivos/' + id;
    } else {
        url = url[0] + '//' + url[2] + '/' + url[3] + '/' + url[4]+ '/arquivos/' + id;
    }

    console.log( url );
    console.log( form );

    $.post(url, form.serialize())
        .done(function (response) {
            swal("Ok! Arquivo deletado com sucesso da base de dados", {
                icon: "success",
            }).then(function(){
                location.reload();
            });
        })
        .fail(function (response) {
            swal({
                title: "Problema!",
                text: "Ocorreu algum erro. Tente novamente.",
                icon: "error",
            });
        });
}