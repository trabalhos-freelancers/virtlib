/**
 * Eventos Gerais
 */
$('#form-new-subject').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  form.loading({
    "message": "Processando requisição"
  });

  $.post(url, $(this).serialize())
    .done(function (response) {
      swal({
        title: "Tudo ok!",
        text: "Novo assunto foi adicionado com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
      document.querySelector('#form-new-subject').reset();
    })
    .fail(function () {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      form.loading('stop');
    });
});

$('#subject-edit-modal #form-edit-subject').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  $.post(url, $(this).serialize())
    .done(function (response) {
      /** obtendo referencia ao modal */
      var modal = $('#subject-edit-modal');
      modal.modal('hide');

      swal({
        title: "Tudo ok!",
        text: "Assunto foi editado com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
    })
    .fail(function () {
      /** obtendo referencia ao modal */
      var modal = $('#subject-edit-modal');
      modal.modal('hide');

      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente em alguns instantes.",
        icon: "error",
      });
      form.loading('stop');
    });
});

/**
 * FIM EVENTOS GERALS
 */

/**
 * Funções GERAIS
 */

function subjectEdit(id) {
  var row = $('#subject-' + id);
  var url = location.href;

  url += '/' + id + '/editar';

  row.loading({
    "message": "Carregando dados..."
  });

  $.get(url)
    .done(function (response) {
      row.loading('stop');

      /** obtendo referencia ao modal */
      var modal = $('#subject-edit-modal');

      /** limpando dados anteriores */
      document.querySelector('#subject-edit-modal #form-edit-subject').reset();

      /** setando action */
      var action =  location.href + '/' + id;
      $('#subject-edit-modal #form-edit-subject').attr('action', action)

      /** populando dados */
      $('#form-edit-subject #subject-name').attr('value', response.name);
      $('#form-edit-subject #subject-last-name').attr('value', response.last_name);

      /** finalmente abrindo modal */
      modal.modal('show');
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      row.loading('stop');
    });
}

function subjectDelete( id ) {
  swal({
    title: "Você tem certeza ?",
    text: "Uma vez deletado você não será capaz de recuperá-lo!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      deletesubjectData( id );
    }
  });
}

function deletesubjectData(id)
{
  var form = $('#form-delete-subject');
  var url = location.href + '/' + id;

  $.post(url, form.serialize())
    .done(function (response) {
      swal("Ok! Assunto deletado com sucesso da base de dados", {
        icon: "success",
      }).then(function(){
        location.reload();
      });
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
    });
}