/**
 * Eventos Gerais
 */
$('#form-new-author').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  form.loading({
    "message": "Processando requisição"
  });

  $.post(url, $(this).serialize())
    .done(function (response) {
      swal({
        title: "Tudo ok!",
        text: "Novo autor foi adicionado com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
      document.querySelector('#form-new-author').reset();
    })
    .fail(function () {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      form.loading('stop');
    });
});

$('#author-edit-modal #form-edit-author').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  $.post(url, $(this).serialize())
    .done(function (response) {
      /** obtendo referencia ao modal */
      var modal = $('#author-edit-modal');
      modal.modal('hide');

      swal({
        title: "Tudo ok!",
        text: "Autor foi editado com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
    })
    .fail(function () {
      /** obtendo referencia ao modal */
      var modal = $('#author-edit-modal');
      modal.modal('hide');

      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente em alguns instantes.",
        icon: "error",
      });
      form.loading('stop');
    });
});

/**
 * FIM EVENTOS GERALS
 */

/**
 * Funções GERAIS
 */

function authorEdit(id) {
  var row = $('#author-' + id);
  var url = location.href;

  url += '/' + id + '/editar';

  row.loading({
    "message": "Carregando autor..."
  });

  $.get(url)
    .done(function (response) {
      row.loading('stop');

      /** obtendo referencia ao modal */
      var modal = $('#author-edit-modal');

      /** limpando dados anteriores */
      document.querySelector('#author-edit-modal #form-edit-author').reset();

      /** setando action */
      var action =  location.href + '/' + id;
      $('#author-edit-modal #form-edit-author').attr('action', action)

      /** populando dados */
      $('#form-edit-author #author-name').attr('value', response.name);
      $('#form-edit-author #author-last-name').attr('value', response.last_name);

      /** finalmente abrindo modal */
      modal.modal('show');
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      row.loading('stop');
    });
}

function authorDelete( id ) {
  swal({
    title: "Você tem certeza ?",
    text: "Uma vez deletado você não será capaz de recuperá-lo!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      deleteAuthorData( id );
    }
  });
}

function deleteAuthorData(id)
{
  var form = $('#form-delete-author');
  var url = location.href + '/' + id;

  $.post(url, form.serialize())
    .done(function (response) {
      swal("Ok! Autor deletado com sucesso da base de dados", {
        icon: "success",
      }).then(function(){
        location.reload();
      });
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
    });
}