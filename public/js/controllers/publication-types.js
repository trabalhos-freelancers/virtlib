/**
 * Eventos Gerais
 */
$('#form-new-publication_type').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  form.loading({
    "message": "Processando requisição"
  });

  $.post(url, $(this).serialize())
    .done(function (response) {
      swal({
        title: "Tudo ok!",
        text: "Novo tipo de publicação foi adicionado com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
      document.querySelector('#form-new-publication_type').reset();
    })
    .fail(function () {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      form.loading('stop');
    });
});

$('#publication_type-edit-modal #form-edit-publication_type').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  $.post(url, $(this).serialize())
    .done(function (response) {
      /** obtendo referencia ao modal */
      var modal = $('#publication_type-edit-modal');
      modal.modal('hide');

      swal({
        title: "Tudo ok!",
        text: "Tipo de publicação foi editado com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
    })
    .fail(function () {
      /** obtendo referencia ao modal */
      var modal = $('#publication_type-edit-modal');
      modal.modal('hide');

      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente em alguns instantes.",
        icon: "error",
      });
      form.loading('stop');
    });
});

/**
 * FIM EVENTOS GERALS
 */

/**
 * Funções GERAIS
 */

function publication_typeEdit(id) {
  var row = $('#publication_type-' + id);
  var url = location.href;

  url += '/' + id + '/editar';

  row.loading({
    "message": "Carregando dados ..."
  });

  $.get(url)
    .done(function (response) {
      row.loading('stop');

      /** obtendo referencia ao modal */
      var modal = $('#publication_type-edit-modal');

      /** limpando dados anteriores */
      document.querySelector('#publication_type-edit-modal #form-edit-publication_type').reset();

      /** setando action */
      var action =  location.href + '/' + id;
      $('#publication_type-edit-modal #form-edit-publication_type').attr('action', action)

      /** populando dados */
      $('#form-edit-publication_type #publication_type-name').attr('value', response.name);
      $('#form-edit-publication_type #publication_type-last-name').attr('value', response.last_name);

      /** finalmente abrindo modal */
      modal.modal('show');
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      row.loading('stop');
    });
}

function publication_typeDelete( id ) {
  swal({
    title: "Você tem certeza ?",
    text: "Uma vez deletado você não será capaz de recuperá-lo!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      deletepublication_typeData( id );
    }
  });
}

function deletepublication_typeData(id)
{
  var form = $('#form-delete-publication_type');
  var url = location.href + '/' + id;

  $.post(url, form.serialize())
    .done(function (response) {
      swal("Ok! Tipo de publicação deletado com sucesso da base de dados", {
        icon: "success",
      }).then(function(){
        location.reload();
      });
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
    });
}