/**
 * Eventos Gerais
 */
$('#form-new-knowledge_area').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  form.loading({
    "message": "Processando requisição"
  });

  $.post(url, $(this).serialize())
    .done(function (response) {
      swal({
        title: "Tudo ok!",
        text: "Nova área de conhecimento foi adicionada com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
      document.querySelector('#form-new-knowledge_area').reset();
    })
    .fail(function () {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      form.loading('stop');
    });
});

$('#knowledge_area-edit-modal #form-edit-knowledge_area').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  $.post(url, $(this).serialize())
    .done(function (response) {
      /** obtendo referencia ao modal */
      var modal = $('#knowledge_area-edit-modal');
      modal.modal('hide');

      swal({
        title: "Tudo ok!",
        text: "Área de conhecimento foi editada com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
    })
    .fail(function () {
      /** obtendo referencia ao modal */
      var modal = $('#knowledge_area-edit-modal');
      modal.modal('hide');

      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente em alguns instantes.",
        icon: "error",
      });
      form.loading('stop');
    });
});

/**
 * FIM EVENTOS GERALS
 */

/**
 * Funções GERAIS
 */

function knowledge_areaEdit(id) {
  var row = $('#knowledge_area-' + id);
  var url = location.href;

  url += '/' + id + '/editar';

  row.loading({
    "message": "Carregando dados..."
  });

  $.get(url)
    .done(function (response) {
      row.loading('stop');

      /** obtendo referencia ao modal */
      var modal = $('#knowledge_area-edit-modal');

      /** limpando dados anteriores */
      document.querySelector('#knowledge_area-edit-modal #form-edit-knowledge_area').reset();

      /** setando action */
      var action =  location.href + '/' + id;
      $('#knowledge_area-edit-modal #form-edit-knowledge_area').attr('action', action)

      /** populando dados */
      $('#form-edit-knowledge_area #knowledge_area-name').attr('value', response.name);
      $('#form-edit-knowledge_area #knowledge_area-last-name').attr('value', response.last_name);

      /** finalmente abrindo modal */
      modal.modal('show');
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      row.loading('stop');
    });
}

function knowledge_areaDelete( id ) {
  swal({
    title: "Você tem certeza ?",
    text: "Uma vez deletado você não será capaz de recuperá-lo!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      deleteknowledge_areaData( id );
    }
  });
}

function deleteknowledge_areaData(id)
{
  var form = $('#form-delete-knowledge_area');
  var url = location.href + '/' + id;

  $.post(url, form.serialize())
    .done(function (response) {
      swal("Ok! Área de conhecimento deletada com sucesso da base de dados", {
        icon: "success",
      }).then(function(){
        location.reload();
      });
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
    });
}