/**
 * Eventos Gerais
 */
$('#form-new-advisor').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  form.loading({
    "message": "Processando requisição"
  });

  $.post(url, $(this).serialize())
    .done(function (response) {
      swal({
        title: "Tudo ok!",
        text: "Novo orientador foi adicionado com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
      document.querySelector('#form-new-advisor').reset();
    })
    .fail(function () {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      form.loading('stop');
    });
});

$('#advisor-edit-modal #form-edit-advisor').submit(function (event) {
  event.preventDefault();

  var url = $(this).attr('action');
  var form = $(this);

  $.post(url, $(this).serialize())
    .done(function (response) {
      /** obtendo referencia ao modal */
      var modal = $('#advisor-edit-modal');
      modal.modal('hide');

      swal({
        title: "Tudo ok!",
        text: "Orientador foi editado com sucesso!",
        icon: "success",
        showCloseButton: false
      }).then(function () {
        location.reload();
      });
    })
    .fail(function () {
      /** obtendo referencia ao modal */
      var modal = $('#advisor-edit-modal');
      modal.modal('hide');

      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente em alguns instantes.",
        icon: "error",
      });
      form.loading('stop');
    });
});

/**
 * FIM EVENTOS GERALS
 */

/**
 * Funções GERAIS
 */

function advisorEdit(id) {
  var row = $('#advisor-' + id);
  var url = location.href;

  url += '/' + id + '/editar';

  row.loading({
    "message": "Carregando orientador..."
  });

  $.get(url)
    .done(function (response) {
      row.loading('stop');

      /** obtendo referencia ao modal */
      var modal = $('#advisor-edit-modal');

      /** limpando dados anteriores */
      document.querySelector('#advisor-edit-modal #form-edit-advisor').reset();

      /** setando action */
      var action =  location.href + '/' + id;
      $('#advisor-edit-modal #form-edit-advisor').attr('action', action)

      /** populando dados */
      $('#form-edit-advisor #advisor-name').attr('value', response.name);
      $('#form-edit-advisor #advisor-last-name').attr('value', response.last_name);

      /** finalmente abrindo modal */
      modal.modal('show');
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
      row.loading('stop');
    });
}

function advisorDelete( id ) {
  swal({
    title: "Você tem certeza ?",
    text: "Uma vez deletado você não será capaz de recuperá-lo!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      deleteadvisorData( id );
    }
  });
}

function deleteadvisorData(id)
{
  var form = $('#form-delete-advisor');
  var url = location.href + '/' + id;

  $.post(url, form.serialize())
    .done(function (response) {
      swal("Ok! Orientador deletado com sucesso da base de dados", {
        icon: "success",
      }).then(function(){
        location.reload();
      });
    })
    .fail(function (response) {
      swal({
        title: "Problema!",
        text: "Ocorreu algum erro. Tente novamente.",
        icon: "error",
      });
    });
}