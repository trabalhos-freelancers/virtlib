<?php 

namespace Virtlib\Repositories;

use Virtlib\Repositories\Interfaces\AdvisorRepositoryInterface;
use Virtlib\Repositories\BaseRepository;
use Virtlib\Models\Advisor;

class AdvisorRepository extends BaseRepository implements AdvisorRepositoryInterface{
    protected $model;

    public function __construct(Advisor $model)
    {
        $this->model = $model;
    }
}
