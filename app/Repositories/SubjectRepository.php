<?php 

namespace Virtlib\Repositories;

use Virtlib\Repositories\Interfaces\SubjectRepositoryInterface;
use Virtlib\Repositories\BaseRepository;
use Virtlib\Models\Subject;

class SubjectRepository extends BaseRepository implements SubjectRepositoryInterface {
    protected $model;

    public function __construct(Subject $model)
    {
        $this->model = $model;
    }
}
