<?php 

namespace Virtlib\Repositories;

use Virtlib\Repositories\Interfaces\KnowledgeAreaRepositoryInterface;
use Virtlib\Repositories\BaseRepository;
use Virtlib\Models\KnowledgeArea;

class KnowledgeAreaRepository extends BaseRepository implements KnowledgeAreaRepositoryInterface{
    protected $model;

    public function __construct(KnowledgeArea $model)
    {
        $this->model = $model;
    }
}
