<?php 

namespace Virtlib\Repositories;

use Virtlib\Repositories\Interfaces\AuthorRepositoryInterface;
use Virtlib\Repositories\BaseRepository;
use Virtlib\Models\Author;

class AuthorRepository extends BaseRepository implements AuthorRepositoryInterface{
    protected $model;

    public function __construct(Author $model)
    {
        $this->model = $model;
    }
}
