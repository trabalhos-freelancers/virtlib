<?php
/**
 * Created by PhpStorm.
 * User: rsouza
 * Date: 11/04/18
 * Time: 17:15
 */

namespace Virtlib\Repositories;

use Illuminate\Support\Facades\Storage;

use Virtlib\Repositories\Interfaces\PublicationRepositoryInterface;
use Virtlib\Repositories\BaseRepository;
use Virtlib\Models\Publication;
use Virtlib\Models\PublicationFile;
use Virtlib\Models\Subject;

class PublicationRepository extends BaseRepository implements PublicationRepositoryInterface
{
    protected $model;

    public function __construct(Publication $model)
    {
        $this->model = $model;
    }

    public function listLastLimitedBy(int $limit)
    {
        return Publication::orderBy('id', 'DESC')->limit($limit)->get();
    }

    public function findByModelAndId($model, int $id)
    {
        if ( $model == 'subject') {
            return ( Subject::find( $id ) )->publications;
        }

        $model = $model."_id";
        return Publication::where($model, $id)->get();
    }

    public function create(array $data)
    {
        $publication = new Publication();

        $publication->fill( $data );

        $publication->author_id = $data['author'];
        $publication->area_id = $data['knowledge_area'];
        $publication->type_id = $data['publication_type'];

        if ( isset( $data['advisor'] ) )
            $publication->advisor_id = $data['advisor']; 

        if ( isset( $data['advisor'] ) && ( $data['advisor'] === 'n/i' ))
            $publication->advisor_id = null;

        if ( $publication->save() ) {
            if ( isset( $data['subjects'] ) )
                $publication->subjects()->attach( $data['subjects'] );

            /** salvando arquivos */
            $files = isset( $data['files'] ) ? $data['files'] : [];
            if ( count( $files ) ) {
                foreach ( $files as $file )
                {
                    $pfile = new PublicationFile();
                    $pfile->publication_id = $publication->id;
                    $pfile->name = 'publicacao-'. date('dmYHis') .'-'. $publication->id .'-'. $publication->advisor_id .'-'. $publication->author_id .'.'. $file->extension();
                    $pfile->path = $file->storeAs('publicacoes', $pfile->name);
                    $pfile->save();
                    sleep(1); /* necessário para gerar valores únicos. Há forma melhor de ser feita ?*/
                }
            }

            $publication->save();
            return $publication;
        } else {
            return null;
        }
    }

    public function update(int $id, array $attributes)
    {
        $publication = $this->findById( $id );

        $publication->fill( $attributes );

        $publication->author_id = $attributes['author'];
        $publication->area_id = $attributes['knowledge_area'];
        $publication->type_id = $attributes['publication_type'];

        if ( isset( $attributes['advisor'] ) )
            $publication->advisor_id = $attributes['advisor']; 

        if ( isset( $attributes['advisor'] ) && ( $attributes['advisor'] === 'n/i' ))
            $publication->advisor_id = null;


        if ( $publication->save() ) {

            if ( $publication->subjects && isset($attributes['subjects'])  ) {
                $publication->subjects()->sync( $attributes['subjects'] );
            } elseif ( !$publication->subjects && isset($attributes['subjects']) ) {
                $publication->subjects()->attach( $attributes['subjects'] );
            } elseif ( !isset( $attributes['subjects']) ) {
                $publication->subjects()->sync([]);
            }

            /** salvando arquivos */
            $files = isset( $attributes['files'] ) ? $attributes['files'] : [];
            if ( count( $files ) ) {
                foreach ( $files as $file )
                {
                    $pfile = new PublicationFile();
                    $pfile->publication_id = $publication->id;
                    $pfile->name = 'publicacao-'. date('dmYHis') .'-'. $publication->id .'-'. $publication->advisor_id .'-'. $publication->author_id .'.'. $file->extension();
                    $pfile->path = $file->storeAs('publicacoes', $pfile->name);
                    $pfile->save();
                    sleep(1); /* necessário para gerar valores únicos. Há forma melhor de ser feita ?*/
                }
            }

            $publication->save();
            return $publication;
        } else {
            return null;
        }
    }

    public function delete(int $id)
    {
        try {
            $publication = $this->findById($id);

            /** deletando arquivos associados do servidor */
            foreach ( $publication->files as $file) {
                Storage::delete($file->path);
            }

            $publication->subjects()->sync([]);
            $publication->files()->delete();
            $publication->delete();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}