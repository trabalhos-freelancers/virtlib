<?php
/**
 * Created by PhpStorm.
 * User: rsouza
 * Date: 13/04/18
 * Time: 13:53
 */

namespace Virtlib\Repositories\Interfaces;


interface PublicationFileRepositoryInterface
{
    public function listAll();
    public function findById(int $id);
    public function create(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}