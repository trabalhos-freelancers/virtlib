<?php

namespace Virtlib\Repositories\Interfaces;

use Virtlib\Repositories\Interfaces\BaseRepositoryInterface;

interface AdvisorRepositoryInterface {
    public function listAll();
    public function findById(int $id);
    public function create(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
