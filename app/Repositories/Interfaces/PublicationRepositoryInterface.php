<?php
/**
 * Created by PhpStorm.
 * User: rsouza
 * Date: 11/04/18
 * Time: 17:13
 */

namespace Virtlib\Repositories\Interfaces;

interface PublicationRepositoryInterface {
    public function listAll();
    public function listLastLimitedBy(int $limit);
    public function findByModelAndId($model, int $id);
    public function findById(int $id);
    public function create(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
