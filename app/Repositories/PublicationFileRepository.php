<?php

namespace Virtlib\Repositories;

use Illuminate\Support\Facades\Storage;
use Virtlib\Repositories\Interfaces\PublicationFileRepositoryInterface;
use Virtlib\Models\PublicationFile;

class PublicationFileRepository extends BaseRepository implements PublicationFileRepositoryInterface
{
    protected $model;

    public function __construct(PublicationFile $model)
    {
        $this->model = $model;
    }

    public function delete(int $id) {
        $file = $this->findById( $id );
        try {
            /** deletando arquivo do servidor */
            Storage::delete($file->path);
            $file->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}