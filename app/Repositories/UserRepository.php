<?php 

namespace Virtlib\Repositories;

use Virtlib\Repositories\Interfaces\UserRepositoryInterface;
use Virtlib\Repositories\BaseRepository;
use Virtlib\Models\User;

class UserRepository extends BaseRepository implements UserRepositoryInterface {
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        $data['password'] = bcrypt($data['password']);
        $user = User::create( $data );

        return $user;
    }

    public function update(int $id, array $data)
    {
        if ( isset($data['password'] ) ) {
            $data['password'] = bcrypt($data['password']);
        }

        $user = User::findOrFail($id);
        $user->fill( $data );
        $user->save();
        return $user;
    }

    public function listLastLimitedBy(int $limit)
    {
        return User::orderBy('id', 'DESC')->limit($limit)->get();
    }
}
