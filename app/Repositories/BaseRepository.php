<?php 

namespace Virtlib\Repositories;

abstract class BaseRepository {
    protected $model;

    public function listAll()
    {
        return $this->model->all();
    }

    public function create(array $attributes)
    {
        return $this->model->create( $attributes );
    }

    public function update(int $id, array $attributes)
    {
        $entity = $this->model->findOrFail( $id );
        $entity->fill( $attributes );
        $entity->save();

        return $entity;
    }

    public function delete(int $id)
    {
        $entity = $this->model->findOrFail( $id );
        
        try {
            $entity->delete();
            return true;
        } catch ( \Exception $e) {
            return false;
        }
    }

    public function findById( int $id )
    {
        $author = $this->model->find( $id );
        if ( $author ) 
        {
            return $author;
        } else {
            return null;
        }
    }
}