<?php 

namespace Virtlib\Repositories;

use Virtlib\Repositories\Interfaces\PublicationTypeRepositoryInterface;
use Virtlib\Repositories\BaseRepository;
use Virtlib\Models\PublicationType;

class PublicationTypeRepository extends BaseRepository implements PublicationTypeRepositoryInterface {
    protected $model;

    public function __construct(PublicationType $model)
    {
        $this->model = $model;
    }
}
