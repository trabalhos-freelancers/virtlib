<?php

namespace Virtlib\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['name', 'status'];

    public function publications()
    {
        return $this->belongsToMany(Publication::class);
    }
}
