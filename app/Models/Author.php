<?php

namespace Virtlib\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = ['name', 'last_name'];
    
    public function publications()
    {
        return $this->hasMany(Publication::class);
    }
}
