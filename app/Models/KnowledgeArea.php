<?php

namespace Virtlib\Models;

use Illuminate\Database\Eloquent\Model;

class KnowledgeArea extends Model
{
    protected $fillable = ['name', 'status'];
    
    public function publications()
    {
        return $this->hasMany(Publication::class, 'area_id');
    }
}
