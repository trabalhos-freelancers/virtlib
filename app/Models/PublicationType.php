<?php

namespace Virtlib\Models;

use Illuminate\Database\Eloquent\Model;

class PublicationType extends Model
{
    protected $fillable = ['name', 'status'];
    
    public function publications()
    {
        return $this->hasMany(Publication::class, 'type_id');
    }
}
