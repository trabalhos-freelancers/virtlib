<?php

namespace Virtlib\Models;

use Illuminate\Database\Eloquent\Model;

class PublicationFile extends Model
{
    public function publication()
    {
        return $this->belongsTo(Publication::class);
    }
}
