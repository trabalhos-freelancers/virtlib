<?php

namespace Virtlib\Models;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    protected $fillable = [
        'title', 'year', 'summary'
    ];

    // Autor
    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    // Orientador
    public function advisor()
    {
        return $this->belongsTo(Advisor::class);
    }

    // Tipo da publicação
    public function type()
    {
        return $this->belongsTo(PublicationType::class, 'type_id');
    }

    // Área de conhecimento
    public function knowledgeArea()
    {
        return $this->belongsTo(KnowledgeArea::class, 'area_id');
    }

    // Arquivos "pdf's" correspondentes
    // Máximo de 3 por publicação 
    public function files()
    {
        return $this->hasMany(PublicationFile::class);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class);
    }
}
