<?php

namespace Virtlib\Models;

use Illuminate\Database\Eloquent\Model;

class Advisor extends Model
{
    protected $fillable = ['name', 'last_name'];
    
    public function publications()
    {
        return $this->hasMany(Publication::class);
    }
}
