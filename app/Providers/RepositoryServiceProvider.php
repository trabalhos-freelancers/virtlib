<?php

namespace Virtlib\Providers;

use Illuminate\Support\ServiceProvider;

use Virtlib\Repositories\Interfaces\AdvisorRepositoryInterface;
use Virtlib\Repositories\Interfaces\AuthorRepositoryInterface;
use Virtlib\Repositories\Interfaces\SubjectRepositoryInterface;
use Virtlib\Repositories\Interfaces\KnowledgeAreaRepositoryInterface;
use Virtlib\Repositories\Interfaces\PublicationTypeRepositoryInterface;
use Virtlib\Repositories\Interfaces\PublicationFileRepositoryInterface;
use Virtlib\Repositories\Interfaces\PublicationRepositoryInterface;
use Virtlib\Repositories\Interfaces\UserRepositoryInterface;
use Virtlib\Repositories\AdvisorRepository;
use Virtlib\Repositories\AuthorRepository;
use Virtlib\Repositories\SubjectRepository;
use Virtlib\Repositories\KnowledgeAreaRepository;
use Virtlib\Repositories\PublicationTypeRepository;
use Virtlib\Repositories\PublicationFileRepository;
use Virtlib\Repositories\PublicationRepository;
use Virtlib\Repositories\UserRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            AuthorRepositoryInterface::class,
            AuthorRepository::class
        );

        $this->app->bind(
            AdvisorRepositoryInterface::class,
            AdvisorRepository::class
        );

        $this->app->bind(
            SubjectRepositoryInterface::class,
            SubjectRepository::class
        );

        $this->app->bind(
            KnowledgeAreaRepositoryInterface::class,
            KnowledgeAreaRepository::class
        );

        $this->app->bind(
            PublicationTypeRepositoryInterface::class,
            PublicationTypeRepository::class
        );

        $this->app->bind(
            PublicationFileRepositoryInterface::class,
            PublicationFileRepository::class
        );

        $this->app->bind(
            PublicationRepositoryInterface::class,
            PublicationRepository::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
    }
}
