<?php

namespace Virtlib\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Virtlib\Repositories\Interfaces\PublicationFileRepositoryInterface;
use Virtlib\Http\Controllers\Controller;

class PublicationFilesController extends Controller
{
    protected $repository;

    public function __construct(PublicationFileRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function destroy( $id )
    {
        if ( $this->repository->delete( $id ) ) {
            return response()->json([
                'status' => 'success',
                'msg' => 'Arquivo deletado com sucesso'
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Ocorreu algum problema. Tente novamente em alguns instantes.'
            ], 400);
        }
    }
}
