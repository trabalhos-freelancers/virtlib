<?php

namespace Virtlib\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Virtlib\Http\Controllers\Controller;
use Virtlib\Repositories\Interfaces\KnowledgeAreaRepositoryInterface;

class KnowledgeAreasController extends Controller
{
    protected $repository;

    public function __construct(KnowledgeAreaRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $knowledge_areas = $this->repository->listAll();
        return view('admin.support.knowledge_areas.index', compact('knowledge_areas'));
    }

    public function create(Request $request)
    {
        return view('admin.support.knowledge_areas.create');
    }

    public function store(Request $request)
    {
        $inputs = $request->validate([
            'name' => 'required|string|max:255'
        ]);

        $knowledge_areas = $this->repository->create( $inputs );

        return response()->json([
                    "message" => "Registro inserido no banco com sucesso!"
                ],200);
    }

    public function edit($id)
    {
        return $this->repository->findById($id);
    }

    public function update(Request $request, $id)
    {
        try {
            $this->repository->update($id, $request->all());
            return response()->json([
                'type' => 'success',
                'msg' => 'Registro atualizado com sucesso!'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'type' => 'error',
                'msg' => 'Ocorreu um erro. Tente novamente em alguns instantes.'
            ], 400);
        }
    }

    public function destroy($id)
    {
        if ( $this->repository->delete($id) ) 
        {
            return response()->json([
                'type' => 'success',
                'msg' => 'Registro atualizado com sucesso!'
            ]);
        } else {
            return response()->json([
                'type' => 'error',
                'msg' => 'Ocorreu um erro. Tente novamente em alguns instantes.'
            ], 400);
        }
    }
}
