<?php

namespace Virtlib\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Virtlib\Http\Controllers\Controller;

use Virtlib\Repositories\Interfaces\PublicationRepositoryInterface;
use Virtlib\Repositories\Interfaces\PublicationTypeRepositoryInterface;
use Virtlib\Repositories\Interfaces\SubjectRepositoryInterface;
use Virtlib\Repositories\Interfaces\KnowledgeAreaRepositoryInterface;
use Virtlib\Repositories\Interfaces\AuthorRepositoryInterface;
use Virtlib\Repositories\Interfaces\AdvisorRepositoryInterface;

class PublicationsController extends Controller
{
    protected $repository;
    protected $subject;
    protected $knowledge_area;
    protected $publication_type;
    protected $author;
    protected $advisor;

    public function __construct(
        PublicationRepositoryInterface $repository,
        SubjectRepositoryInterface $subject,
        KnowledgeAreaRepositoryInterface $knowledge_area,
        PublicationTypeRepositoryInterface $publication_type,
        AuthorRepositoryInterface $author,
        AdvisorRepositoryInterface $advisor
    )
    {
        $this->repository = $repository;
        $this->subject = $subject;
        $this->knowledge_area = $knowledge_area;
        $this->publication_type = $publication_type;
        $this->author = $author;
        $this->advisor = $advisor;
    }

    public function index(Request $request)
    {
        if ( $request->has('modelo') && $request->has('id') ) {
            $model = $request->input('modelo');
            $id = $request->input('id');
            $publications = $this->repository->findByModelAndId($model, $id);
        } else {
            $publications = $this->repository->listAll();
        }
        
        return view('admin.publications.index', compact('publications'));
    }

    public function show($id)
    {
        $publication = $this->repository->findById( $id );

        if ( $publication )
        {
            $publication->author = $publication->author->name;
            if ( $publication->advisor ) $publication->advisor = $publication->advisor->name;
            $publication->knowledgeArea = $publication->knowledgeArea->name;
            $publication->type = $publication->type->name;
            $publication->files = $publication->files;

            $subjects = [];
            foreach( $publication->subjects as $subject ){
                $subjects[] = $subject->name;
            }

            $publication->subjects = $subjects;
        }

        return $publication;
    }

    public function create()
    {
        $subjects = $this->subject->listAll();
        $knowledge_areas = $this->knowledge_area->listAll();
        $publication_types = $this->publication_type->listAll();
        $authors = $this->author->listAll();
        $advisors = $this->advisor->listAll();

        return view('admin.publications.create', compact(
            'subjects',
            'knowledge_areas',
            'publication_types',
            'authors',
            'advisors'
        ));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'author' => 'required',
            'year' => 'required',
        ]);
        
        if ( $this->repository->create($request->all()) ) {
            return response()->json([
                'status' => 'success',
                'msg' => 'Publicação adicionada com sucesso a base de dados'
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Ocorreu algum problema. Tente novamente em alguns instantes.'
            ], 400);
        }
    }

    public function edit($id) {
        $publication = $this->repository->findById($id);
        $subjects = $this->subject->listAll();
        $knowledge_areas = $this->knowledge_area->listAll();
        $publication_types = $this->publication_type->listAll();
        $authors = $this->author->listAll();
        $advisors = $this->advisor->listAll();

        return view('admin.publications.edit', compact(
            'publication',
            'subjects',
            'knowledge_areas',
            'publication_types',
            'authors',
            'advisors'
        ));
    }

    public function update(Request $request, $id) {
        try {
            $this->repository->update($id, $request->all());
            return response()->json([
                'status' => 'success',
                'msg' => 'Publicação atualizada com sucesso'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Ocorreu algum problema. Tente novamente em alguns instantes.'
            ], 400);
        }
    }

    public function destroy($id)
    {
        $this->repository->delete( $id );
        return response()->json([
            'status' => 'success',
            'msg' => 'Publicação deletada com sucesso'
        ], 200);
    }
}
