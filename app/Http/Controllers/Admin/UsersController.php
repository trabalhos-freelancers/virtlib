<?php

namespace Virtlib\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Virtlib\Http\Controllers\Controller;

use Virtlib\Repositories\Interfaces\UserRepositoryInterface;

class UsersController extends Controller
{
    protected $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $users = $this->repository->listAll();
        return view('admin.users.index', compact('users'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = $this->repository->create( $request->all() );

        return response()->json([
                    "message" => "Registro inserido no banco com sucesso!"
                ],200);
    }

    public function edit($id)
    {
        $user = $this->repository->findById( $id );
        if ( $user ) {
            return $user;
        } else {
            return response()->json(
                ['message' => "Não há registro na base de dados para o valor passado"], 
                404
            );
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $this->repository->update($id, $request->all());
            return response()->json([
                'type' => 'success',
                'msg' => 'Registro atualizado com sucesso!'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'type' => 'error',
                'msg' => 'Ocorreu um erro. Tente novamente em alguns instantes.'
            ], 400);
        }
    }

    public function destroy($id)
    {
        if ( $this->repository->delete($id) ) 
        {
            return response()->json([
                'type' => 'success',
                'msg' => 'Registro atualizado com sucesso!'
            ]);
        } else {
            return response()->json([
                'type' => 'error',
                'msg' => 'Ocorreu um erro. Tente novamente em alguns instantes.'
            ], 400);
        }
    }
}
