<?php

namespace Virtlib\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Virtlib\Http\Controllers\Controller;

use Virtlib\Repositories\Interfaces\PublicationRepositoryInterface;
use Virtlib\Repositories\Interfaces\PublicationFileRepositoryInterface;
use Virtlib\Repositories\Interfaces\UserRepositoryInterface;

class AdminController extends Controller
{
    protected $publication;
    protected $file;
    protected $user;

    public function __construct(
        PublicationRepositoryInterface $publication,
        PublicationFileRepositoryInterface $file,
        UserRepositoryInterface $user
    ) {
        $this->publication = $publication;
        $this->file = $file;
        $this->user = $user;
    }

    public function index()
    {
        $publications_count = count($this->publication->listAll());
        $files_count = count($this->file->listAll());
        $users_count = count($this->user->listAll());
        $last_publications = $this->publication->listLastLimitedBy(7);

        return view('admin.dashboard', compact(
            'publications_count',
            'files_count',
            'users_count',
            'last_publications'
        ));
    }
}
