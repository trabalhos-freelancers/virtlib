<?php

namespace Virtlib\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Virtlib\Http\Controllers\Controller;
use Virtlib\Repositories\Interfaces\PublicationRepositoryInterface;

use Virtlib\Models\PublicationType;
use Virtlib\Models\KnowledgeArea;

class PublicationsController extends Controller
{
    protected $repository;

    public function __construct(PublicationRepositoryInterface $repository)
    {
        $this->repository = $repository;
        View::share('publication_types', PublicationType::all());
        View::share('knowledge_areas', KnowledgeArea::all());
    }

    public function download($path)
    {
        $file = 'publicacoes/' . $path;
        return Storage::download($file);
    }

    public function show( $id )
    {
        $publication = $this->repository->findById( $id );
        return view('front.show', compact('publication'));
    }
}
