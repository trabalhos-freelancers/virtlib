<?php

namespace Virtlib\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Virtlib\Http\Controllers\Controller;

use Virtlib\Models\Author;
use Virtlib\Models\Advisor;
use Virtlib\Models\Publication;
use Virtlib\Models\PublicationType;
use Virtlib\Models\KnowledgeArea;
use Virtlib\Repositories\Interfaces\PublicationRepositoryInterface;
use Virtlib\Repositories\Interfaces\AuthorRepositoryInterface;

class HomeController extends Controller
{
    protected $repository;
    protected $author;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PublicationRepositoryInterface $repository, AuthorRepositoryInterface $author)
    {
        // $this->middleware('auth');
        $this->repository = $repository;
        $this->author = $author;

        View::share('publication_types', PublicationType::all());
        View::share('knowledge_areas', KnowledgeArea::all());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publications = $this->repository->listLastLimitedBy(2);
        return view('front.home', compact('publications'));
    }

    public function searchByAuthor(Request $request)
    {
        if ( $request->has('filtro') ) {
            $author = Author::find( $request->input('filtro'));
            $publications = Publication::where('author_id', $request->input('filtro'))->paginate(5);
            return view('front.authors.search_by_authors_filtred', compact( 'author', 'publications'));
        }

        if ( $request->has('nome') ) {
            $authors = Author::where('name', 'like', '%'.$request->input('nome').'%')
                ->orWhere('last_name', 'like','%'.$request->input('nome').'%')->paginate(10);
            return view('front.authors.search_by_authors', compact('authors'));
        }

        $authors = Author::paginate(10);
        return view('front.authors.search_by_authors', compact('authors'));
    }

    public function searchByAdvisor(Request $request)
    {
        if ( $request->has('filtro') ) {
            $advisor = Advisor::find( $request->input('filtro'));
            $publications = Publication::where('advisor_id', $request->input('filtro'))->paginate(5);
            return view('front.advisors.search_by_advisors_filtred', compact( 'advisor', 'publications'));
        }

        if ( $request->has('nome') ) {
            $advisors = Advisor::where('name', 'like', '%'.$request->input('nome').'%')
                                ->orWhere('last_name', 'like','%'.$request->input('nome').'%')->paginate(10);
            return view('front.advisors.search_by_advisors', compact('advisors'));
        }

        $advisors = Advisor::paginate(10);
        return view('front.advisors.search_by_advisors', compact('advisors'));
    }

    public function searchByTitle(Request $request)
    {
        if ( $request->has('filtro') || $request->has('tipo') || $request->has('area') ) {
            $queries = [];
            if ( $request->has('filtro') && $request->input('filtro') ) {
                $title = $request->input('filtro');
                $queries[] = ['title', 'like', '%'.$request->input('filtro').'%'];
            } else {
                $title = "Todas as publicações";
            }
            if ( $request->has('tipo') && $request->input('tipo') ) $queries[] = ['type_id', $request->input('tipo')]; 
            if ( $request->has('area') && $request->input('area') ) $queries[] = ['area_id', $request->input('area')]; 
            $publications = Publication::where( $queries )->paginate(5);
            return view('front.publications.search_by_titles_filtred', compact( 'title', 'publications'));
        }

        if ( $request->has('titulo') ) {
            $publications = Publication::where('name', 'like', '%'.$request->input('titulo').'%')->paginate(10);
            return view('front.publications.search_by_titles', compact('publications'));
        }

        $publications = Publication::paginate(10);
        return view('front.publications.search_by_titles', compact('publications'));
    }
}
