-- MySQL dump 10.13  Distrib 5.7.19, for Win64 (x86_64)
--
-- Host: localhost    Database: virtlib
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advisors`
--

DROP TABLE IF EXISTS `advisors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advisors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advisors`
--

LOCK TABLES `advisors` WRITE;
/*!40000 ALTER TABLE `advisors` DISABLE KEYS */;
INSERT INTO `advisors` VALUES (1,'Victor','Schroeder','2018-04-23 23:56:57','2018-04-23 23:56:57'),(2,'Berry','Paucek','2018-04-23 23:56:57','2018-04-23 23:56:57'),(3,'Frankie','Haley','2018-04-23 23:56:57','2018-04-23 23:56:57'),(4,'Juwan','Bruen','2018-04-23 23:56:57','2018-04-23 23:56:57'),(5,'Jay','Zulauf','2018-04-23 23:56:57','2018-04-23 23:56:57'),(6,'Kobe','Yost','2018-04-23 23:56:57','2018-04-23 23:56:57'),(7,'Carmella','Klein','2018-04-23 23:56:57','2018-04-23 23:56:57'),(8,'Uriel','Monahan','2018-04-23 23:56:57','2018-04-23 23:56:57'),(9,'Nick','Predovic','2018-04-23 23:56:57','2018-04-23 23:56:57'),(10,'Elyssa','Corwin','2018-04-23 23:56:57','2018-04-23 23:56:57'),(11,'Donald','Leannon','2018-04-23 23:56:57','2018-04-23 23:56:57'),(12,'Roxanne','Mraz','2018-04-23 23:56:57','2018-04-23 23:56:57'),(13,'Hershel','Brekke','2018-04-23 23:56:57','2018-04-23 23:56:57'),(14,'Brandi','Runolfsdottir','2018-04-23 23:56:57','2018-04-23 23:56:57'),(15,'Bella','Zemlak','2018-04-23 23:56:57','2018-04-23 23:56:57'),(16,'Hector','Keebler','2018-04-23 23:56:57','2018-04-23 23:56:57'),(17,'Maximus','Feeney','2018-04-23 23:56:57','2018-04-23 23:56:57'),(18,'Jerald','Klein','2018-04-23 23:56:57','2018-04-23 23:56:57'),(19,'Margarett','Schaefer','2018-04-23 23:56:57','2018-04-23 23:56:57'),(20,'Melyna','Kiehn','2018-04-23 23:56:57','2018-04-23 23:56:57');
/*!40000 ALTER TABLE `advisors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,'Kraig','King','2018-04-23 23:56:56','2018-04-23 23:56:56'),(2,'Celestino','Gutmann','2018-04-23 23:56:56','2018-04-23 23:56:56'),(3,'Ceasar','Botsford','2018-04-23 23:56:56','2018-04-23 23:56:56'),(4,'Dandre','Thompson','2018-04-23 23:56:56','2018-04-23 23:56:56'),(5,'Joyce','Lebsack','2018-04-23 23:56:56','2018-04-23 23:56:56'),(6,'Kolby','Balistreri','2018-04-23 23:56:56','2018-04-23 23:56:56'),(7,'Eda','Jast','2018-04-23 23:56:56','2018-04-23 23:56:56'),(8,'Virginie','Hahn','2018-04-23 23:56:56','2018-04-23 23:56:56'),(9,'Brandon','Kuhic','2018-04-23 23:56:56','2018-04-23 23:56:56'),(10,'Joanie','Goyette','2018-04-23 23:56:56','2018-04-23 23:56:56'),(11,'Cindy','Schuppe','2018-04-23 23:56:56','2018-04-23 23:56:56'),(12,'Litzy','Wunsch','2018-04-23 23:56:56','2018-04-23 23:56:56'),(13,'Friedrich','Haag','2018-04-23 23:56:56','2018-04-23 23:56:56'),(14,'Emily','Olson','2018-04-23 23:56:56','2018-04-23 23:56:56'),(15,'Laurel','Stoltenberg','2018-04-23 23:56:56','2018-04-23 23:56:56'),(16,'Kaylie','Torphy','2018-04-23 23:56:56','2018-04-23 23:56:56'),(17,'Burdette','Murray','2018-04-23 23:56:56','2018-04-23 23:56:56'),(18,'Juvenal','Frami','2018-04-23 23:56:56','2018-04-23 23:56:56'),(19,'Alana','Wehner','2018-04-23 23:56:56','2018-04-23 23:56:56'),(20,'Era','Rodriguez','2018-04-23 23:56:56','2018-04-23 23:56:56'),(21,'Cletus','Beahan','2018-04-23 23:56:56','2018-04-23 23:56:56'),(22,'Elise','Gottlieb','2018-04-23 23:56:56','2018-04-23 23:56:56'),(23,'Emie','Watsica','2018-04-23 23:56:56','2018-04-23 23:56:56'),(24,'Sasha','Watsica','2018-04-23 23:56:56','2018-04-23 23:56:56'),(25,'Mac','Haley','2018-04-23 23:56:56','2018-04-23 23:56:56'),(26,'Roel','Eichmann','2018-04-23 23:56:56','2018-04-23 23:56:56'),(27,'Xzavier','Schmidt','2018-04-23 23:56:56','2018-04-23 23:56:56'),(28,'Armani','Hayes','2018-04-23 23:56:57','2018-04-23 23:56:57'),(29,'Dylan','Nicolas','2018-04-23 23:56:57','2018-04-23 23:56:57'),(30,'Loyal','Smith','2018-04-23 23:56:57','2018-04-23 23:56:57');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `knowledge_areas`
--

DROP TABLE IF EXISTS `knowledge_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `knowledge_areas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `knowledge_areas`
--

LOCK TABLES `knowledge_areas` WRITE;
/*!40000 ALTER TABLE `knowledge_areas` DISABLE KEYS */;
INSERT INTO `knowledge_areas` VALUES (1,'She.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(2,'Time as.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(3,'Latin.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(4,'Hatter.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(5,'I.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(6,'March.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(7,'.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(8,'Alice,).',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(9,'Alice.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(10,'I didn\'t.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(11,'But said.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(12,'King..',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(13,'Alice.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(14,'The.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(15,'Rabbit.',1,'2018-04-23 23:57:05','2018-04-23 23:57:05');
/*!40000 ALTER TABLE `knowledge_areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (12,'2014_10_12_000000_create_users_table',1),(13,'2014_10_12_100000_create_password_resets_table',1),(14,'2018_03_31_011232_create_authors_table',1),(15,'2018_03_31_011311_create_advisors_table',1),(16,'2018_03_31_011344_create_publication_types_table',1),(17,'2018_03_31_011446_create_knowledge_areas_table',1),(18,'2018_03_31_011942_create_publications_table',1),(19,'2018_03_31_012137_create_publication_files_table',1),(20,'2018_04_08_200743_create_subjects_table',1),(21,'2018_04_10_182733_create_publication_subject_table',1),(22,'2018_04_12_122108_create_permission_tables',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publication_files`
--

DROP TABLE IF EXISTS `publication_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publication_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `publication_files_publication_id_foreign` (`publication_id`),
  CONSTRAINT `publication_files_publication_id_foreign` FOREIGN KEY (`publication_id`) REFERENCES `publications` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publication_files`
--

LOCK TABLES `publication_files` WRITE;
/*!40000 ALTER TABLE `publication_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `publication_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publication_subject`
--

DROP TABLE IF EXISTS `publication_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication_subject` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publication_id` int(10) unsigned NOT NULL,
  `subject_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `publication_subject_publication_id_foreign` (`publication_id`),
  KEY `publication_subject_subject_id_foreign` (`subject_id`),
  CONSTRAINT `publication_subject_publication_id_foreign` FOREIGN KEY (`publication_id`) REFERENCES `publications` (`id`),
  CONSTRAINT `publication_subject_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publication_subject`
--

LOCK TABLES `publication_subject` WRITE;
/*!40000 ALTER TABLE `publication_subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `publication_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publication_types`
--

DROP TABLE IF EXISTS `publication_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publication_types`
--

LOCK TABLES `publication_types` WRITE;
/*!40000 ALTER TABLE `publication_types` DISABLE KEYS */;
INSERT INTO `publication_types` VALUES (1,'iusto',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(2,'ut',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(3,'atque',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(4,'quia',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(5,'veniam',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(6,'sit',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(7,'et',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(8,'totam',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(9,'aliquid',1,'2018-04-23 23:57:05','2018-04-23 23:57:05'),(10,'accusantium',1,'2018-04-23 23:57:05','2018-04-23 23:57:05');
/*!40000 ALTER TABLE `publication_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publications`
--

DROP TABLE IF EXISTS `publications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `advisor_id` int(10) unsigned DEFAULT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `area_id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` mediumtext COLLATE utf8mb4_unicode_ci,
  `year` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `publications_author_id_foreign` (`author_id`),
  KEY `publications_advisor_id_foreign` (`advisor_id`),
  KEY `publications_type_id_foreign` (`type_id`),
  KEY `publications_area_id_foreign` (`area_id`),
  CONSTRAINT `publications_advisor_id_foreign` FOREIGN KEY (`advisor_id`) REFERENCES `advisors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `publications_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `knowledge_areas` (`id`) ON DELETE CASCADE,
  CONSTRAINT `publications_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `publications_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `publication_types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publications`
--

LOCK TABLES `publications` WRITE;
/*!40000 ALTER TABLE `publications` DISABLE KEYS */;
/*!40000 ALTER TABLE `publications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` VALUES (1,'fuga',1,'2018-04-23 23:56:57','2018-04-23 23:56:57'),(2,'aperiam',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(3,'sint',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(4,'tempora',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(5,'tenetur',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(6,'placeat',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(7,'quam',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(8,'sunt',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(9,'ex',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(10,'ipsum',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(11,'dicta',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(12,'adipisci',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(13,'nobis',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(14,'ea',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(15,'consequatur',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(16,'nisi',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(17,'accusantium',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(18,'a',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(19,'dignissimos',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(20,'atque',1,'2018-04-23 23:56:58','2018-04-23 23:56:58'),(21,'aut',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(22,'voluptatem',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(23,'aut',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(24,'nemo',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(25,'qui',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(26,'sunt',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(27,'odio',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(28,'ab',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(29,'molestiae',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(30,'vero',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(31,'vel',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(32,'commodi',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(33,'quae',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(34,'temporibus',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(35,'inventore',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(36,'minus',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(37,'aut',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(38,'tenetur',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(39,'magni',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(40,'qui',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(41,'illum',1,'2018-04-23 23:56:59','2018-04-23 23:56:59'),(42,'voluptatum',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(43,'repellendus',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(44,'quis',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(45,'quas',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(46,'nihil',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(47,'quibusdam',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(48,'facere',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(49,'voluptate',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(50,'ipsam',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(51,'ut',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(52,'quos',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(53,'laborum',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(54,'quae',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(55,'quisquam',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(56,'quae',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(57,'alias',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(58,'repellat',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(59,'eos',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(60,'et',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(61,'sed',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(62,'quisquam',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(63,'iusto',1,'2018-04-23 23:57:00','2018-04-23 23:57:00'),(64,'molestias',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(65,'ipsa',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(66,'cum',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(67,'nesciunt',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(68,'eos',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(69,'natus',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(70,'delectus',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(71,'expedita',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(72,'praesentium',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(73,'vel',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(74,'porro',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(75,'numquam',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(76,'unde',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(77,'dolorem',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(78,'et',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(79,'molestias',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(80,'aperiam',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(81,'qui',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(82,'quia',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(83,'omnis',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(84,'itaque',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(85,'amet',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(86,'ut',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(87,'tempora',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(88,'nobis',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(89,'dolor',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(90,'et',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(91,'suscipit',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(92,'rerum',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(93,'voluptatem',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(94,'placeat',1,'2018-04-23 23:57:01','2018-04-23 23:57:01'),(95,'consequatur',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(96,'quia',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(97,'eaque',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(98,'dolores',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(99,'quo',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(100,'officiis',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(101,'quos',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(102,'iusto',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(103,'quidem',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(104,'repellat',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(105,'culpa',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(106,'odio',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(107,'ipsum',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(108,'voluptatem',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(109,'reprehenderit',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(110,'quis',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(111,'ea',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(112,'nemo',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(113,'aut',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(114,'mollitia',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(115,'nulla',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(116,'nihil',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(117,'at',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(118,'hic',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(119,'assumenda',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(120,'quisquam',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(121,'est',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(122,'aut',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(123,'rem',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(124,'deleniti',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(125,'aliquam',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(126,'ducimus',1,'2018-04-23 23:57:02','2018-04-23 23:57:02'),(127,'eligendi',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(128,'in',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(129,'tenetur',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(130,'inventore',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(131,'voluptatum',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(132,'impedit',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(133,'placeat',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(134,'eos',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(135,'voluptatibus',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(136,'tenetur',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(137,'magnam',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(138,'et',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(139,'blanditiis',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(140,'aspernatur',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(141,'alias',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(142,'placeat',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(143,'deleniti',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(144,'voluptatem',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(145,'et',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(146,'iste',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(147,'est',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(148,'dolore',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(149,'id',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(150,'est',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(151,'perferendis',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(152,'molestiae',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(153,'repellat',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(154,'soluta',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(155,'id',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(156,'ut',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(157,'architecto',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(158,'delectus',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(159,'rerum',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(160,'voluptatem',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(161,'accusantium',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(162,'qui',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(163,'et',1,'2018-04-23 23:57:03','2018-04-23 23:57:03'),(164,'veniam',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(165,'at',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(166,'nulla',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(167,'aut',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(168,'et',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(169,'ut',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(170,'eveniet',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(171,'sapiente',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(172,'et',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(173,'quis',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(174,'at',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(175,'quo',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(176,'accusamus',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(177,'quidem',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(178,'ducimus',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(179,'quia',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(180,'omnis',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(181,'fuga',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(182,'enim',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(183,'repellat',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(184,'aut',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(185,'repudiandae',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(186,'nostrum',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(187,'quas',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(188,'qui',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(189,'qui',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(190,'et',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(191,'nostrum',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(192,'placeat',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(193,'rerum',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(194,'quia',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(195,'porro',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(196,'molestiae',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(197,'impedit',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(198,'nisi',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(199,'dolorem',1,'2018-04-23 23:57:04','2018-04-23 23:57:04'),(200,'et',1,'2018-04-23 23:57:05','2018-04-23 23:57:05');
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Administrador','admin@admin.com','$2y$10$NCng35i9TNhcpvxaNrhLI.btzE8EUiLs1TLVpX5lSkVU2KquZ6ONi','eBg9ZpxHj7',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-23 18:01:55
