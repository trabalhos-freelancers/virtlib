let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/** Copiando assets necessários */
mix.copy('./node_modules/axios/dist/axios.min.js', './public/js');
mix.copy('./node_modules/jquery/dist/jquery.min.js', './public/js');
mix.copy('./node_modules/popper.js/dist/popper.js.min.js', './public/js');
mix.copy('./node_modules/bootstrap/dist/bootstrap.bundle.min.js', './public/js');
mix.copy('./node_modules/bootstrap/dist/bootstrap.min.js', './public/js');

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
