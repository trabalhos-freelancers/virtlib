<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Rotas públicas */
Route::group(['namespace' => 'Front'], function () {
    Route::get('/', 'HomeController@index');

    Route::get('donwload/{path}', 'PublicationsController@download')->name('public.publications.download');
    Route::get('publicacao/{id}', 'PublicationsController@show')->name('public.publication.show');

    Route::group(['prefix' => 'pesquisar'], function() {
        Route::get('autores', 'HomeController@searchByAuthor')->name('public.search.authors');
        Route::get('orientadores', 'HomeController@searchByAdvisor')->name('public.search.advisors');
        Route::get('titulos', 'HomeController@searchByTitle')->name('public.search.titles');
        Route::get('datas', 'HomeController@searchByDate')->name('public.search.dates');
    });
});

/** Rotas administrativas */
Route::group([
    'prefix' => 'administracao',
    'namespace' => 'Admin',
    'middleware' => ['auth'] /** linha da autenticação | apenas usuários logados podem acessar o admin */
], function () {
    Route::get('/', 'AdminController@index');
    Route::resource('arquivos', 'PublicationFilesController');
    Route::resource('publicacoes', 'PublicationsController');
    
    Route::group(['prefix' => 'cadastros'], function () {
        Route::resource('areas-de-conhecimento', 'KnowledgeAreasController');
        Route::resource('tipos-de-publicacao', 'PublicationTypesController');
        Route::resource('assuntos', 'SubjectsController');
        Route::resource('orientadores', 'AdvisorsController');
        Route::resource('autores', 'AuthorsController');
        Route::resource('usuarios', 'UsersController');
    });
});

Auth::routes();

/** Fix Temporário */
Route::get('/register', function() { return redirect('/'); });
